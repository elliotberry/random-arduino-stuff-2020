const vorpal = require('vorpal')();
var colors = require('colors');

var save = require("./inc/mango.js");

//cli commands
vorpal
    .command('start', 'Starts new timer.')
    .option('-n, --name', 'Required: project name.')
    .action(function (args, callback) {
        console.log(args);
        callback();
    });
vorpal.command('stop', 'Stops timer')
    .action(function (args, callback) {
        console.log(args);
        callback();
    });
vorpal.command('status', 'Shows all projects with times.')
    .action(function (args, callback) {
        console.log(args);
        callback();
    });

vorpal
    .delimiter('timah$')
    .show();