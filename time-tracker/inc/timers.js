const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({
            filename: 'error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: 'combined.log'
        })
    ]
});


var simpleTimer = require('node-timers/simple');
var config = require('config.json')
var timers = []; //array of all timers;

function createTimer(task, ID) {
    var myTimer = simpleTimer.simple();
    logger.info('started timer at ');
    myTimer.start();
}

function stopTimer(task, ID) {
    logger.info('stopped timer at ');
}