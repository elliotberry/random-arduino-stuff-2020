var request = require("request");
var bodyParser = require('body-parser');
var token; //store token in a sec
var cookie; //store cookie in a sec
var projArr = [];
var finalArr = [];
var end = false;

var baseOptions = {
    json: true,
    gzip: true,
    method: 'GET',
    url: 'https://app.10000ft.com/api/sessions/signin',
    headers: {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
        'accept-language': 'en-US,en;q=0.9'
    },
};
var opp = JSON.parse(JSON.stringify(baseOptions));
opp.body = '{"user_id":"eberry@tankdesign.com","password":"Cc81m4xTSL"}';

request(opp, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(body);
    console.log(response.set-cookie);
    console.log(body)
    token = response.auth_token;
    cookie = response.set_cookie;
    //getTheProjects();
});


function getTheProjects() {
    var options = {
        json: true,

        method: 'GET',
        url: 'https://app.10000ft.com/api/v1/users/438097/time_entries?with_suggestions=true&fields=approvals&from=2018-11-26&to=2018-12-2&per_page=1000&page=1',
        headers: {
            'cache-control': 'no-cache,no-cache',
            accept: 'application/json, text/javascript, */*; q=0.01',
            'content-type': 'application/json',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36',
            pragma: 'no-cache',
            cookie: cookie,
            'x-requested-with': 'XMLHttpRequest',
            'accept-language': 'en-US,en;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            origin: 'https://app.10000ft.com'
        },
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        console.log(body.toString());
        console.log(response);
        //projArr = body.data;
        //getTheProjectData();
    });

}

function getTheProjectData() {
    for (x=0; x < projArr.length; x++) {
        if (x == projArr.length) {
            end = true;
        }
        getAProjectObj(projArr[x].assignable_id, end);
    }
}

function getAProjectObj(id, end) {
    var options = {
        method: 'GET',
        json: true,
        url: 'https: //app.10000ft.com/api/v1/projects/' + id,
        headers: {
            'cache-control': 'no-cache,no-cache',
            accept: 'application/json, text/javascript, */*; q=0.01',
            'content-type': 'application/json',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36',
            pragma: 'no-cache',
            cookie: cookie,
            'x-requested-with': 'XMLHttpRequest',
            'accept-language': 'en-US,en;q=0.9',
            'accept-encoding': 'gzip, deflate, br',
            origin: 'https://app.10000ft.com'
        },
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        var thisProj = {};
        finalArr.push(body);
    });
    if (end == true) {
        end();
    }
}

function end() {
    console.log("done, here's yer array: ");
    console.log(finalArr);
}
