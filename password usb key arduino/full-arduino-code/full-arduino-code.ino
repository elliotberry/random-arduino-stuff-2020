// Bad usb with three buttons. when one is pressed, a password is printed with keyboard library.
//
//
//global vars
int numberOfItems = 2;
int debounceTime = 50;

int pins[] = {18, 19, 20};
boolean lastValue[] = {HIGH, HIGH, HIGH};
long countSinceLast[] = {debounceTime, debounceTime, debounceTime};

char code1[] = "One";
char code2[] = "Two";
char code3[] = "Three";


#include <Keyboard.h>

void setup() {
  for (int z=0; z<numberOfItems; z++) {
    pinMode(pins[z], INPUT_PULLUP);
  }
  // initialize control over the keyboard:
  Keyboard.begin();

}

void loop() {
  for (int t=0; t<numberOfItems; t++) {

    //read the pin
    boolean currentVal = digitalRead(pins[t]);
    //see if it's different than last time
    if (currentVal != lastValue[t] && countSinceLast[t] >= debounceTime) {
      if (t == 0) {
        Keyboard.print(code1);
      }
      if (t == 1) {
          Keyboard.print(code2);
      }
     if (t == 2) {
         Keyboard.print(code3);
      }
      countSinceLast[t] = 0;
    }

    if (countSinceLast[t] != 0 && countSinceLast[t] != 500) {
    
      countSinceLast[t]++;
    }
    
    lastValue[t] = currentVal;
 }

}
