// Bad usb with three buttons. when one is pressed, a password is printed with keyboard library.
//
//
//global vars

int debounceTime = 100;
int triggerRange = 50;

boolean timerOn = false;
boolean debouncing = false;
long debounceTimer = 0;

int pin = 18;
boolean lastValue = HIGH;
long countSinceLast[] = debounceTime;

int numberOfClicks = 0;

char code1[] = "One";
char code2[] = "Two";
char code3[] = "Three";


#include <Keyboard.h>

void setup() {

  pinMode(pin, INPUT_PULLUP);
 
  // initialize control over the keyboard:
  Keyboard.begin();

}

void loop() {
   //read the pin
   boolean currentVal = digitalRead(pin);

  if (currentVal != lastVal) {
    if (timerOn = true && debouncing = false) {
      numberOfClicks++;
    }
  }

   
  if (timerOn = true) {
    if (debouncing = true) {
      if (debounceTimer >= debounceTime) {
        debounceTime = 0;
        debounceTimer = false;
      }
      else {
        debounceTimer++;
      }
    }
  }

  lastValue = lastValue;
}
