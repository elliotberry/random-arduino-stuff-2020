int pin1 = 18; //a0
int pin2 = 19; //a1
int pin3 = 20; //a2

#include <Keyboard.h>

void setup() {

  pinMode(pin1, INPUT_PULLUP);
  pinMode(pin2, INPUT_PULLUP);
  pinMode(pin3, INPUT_PULLUP);

  // initialize control over the keyboard:
  Keyboard.begin();
}

void loop(){
  int val1 = digitalRead(pin1); 
  int val2 = digitalRead(pin2); 
  int val3 = digitalRead(pin3); 
  
  if (val1 == LOW) {        
    Keyboard.print("One");
  } 
  if (val2 == LOW) {        
   Keyboard.print("Two");
  } 
  if (val3 == LOW) {        
   Keyboard.print("Three");
  }
  
}
